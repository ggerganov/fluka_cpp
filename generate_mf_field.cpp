#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>

using namespace std;

int main(int argc, char **argv) {
  if (argc < 8) {
    cerr << "Usage: " << argv[0] << " MFfilename nx ny nz dx dy dz" << endl;
    return 1;
  } 

  int nx = atoi(argv[2]);
  int ny = atoi(argv[3]);
  int nz = atoi(argv[4]);
  double dx = atof(argv[5]);
  double dy = atof(argv[6]);
  double dz = atof(argv[7]);

  ofstream fout(argv[1], ios::binary);
  fout.write((char *)(&nx), sizeof(nx));
  fout.write((char *)(&ny), sizeof(ny));
  fout.write((char *)(&nz), sizeof(nz));
  fout.write((char *)(&dx), sizeof(dx));
  fout.write((char *)(&dy), sizeof(dy));
  fout.write((char *)(&dz), sizeof(dz));

  for (int z = 0; z < nz; ++z) {
    for (int y = 0; y < ny; ++y) {
      for (int x = 0; x < nx; ++x) {
        double u = (double(x) - double(nx-1)/2.0);
        double v = (double(y) - double(ny-1)/2.0);
        //double u = 1;
        //double v = 0;
        double w = 0;
        double l = sqrt(u*u + v*v + w*w);
        double b;
        if (l != 0) { 
          u /= l; v /= l; w /= l;
          b = 1;
        }
        else {
          b = 0;
        }
        fout.write((char *)(&u), sizeof(u));
        fout.write((char *)(&v), sizeof(v));
        fout.write((char *)(&w), sizeof(w));
        fout.write((char *)(&b), sizeof(b));
      }
    }
  }

  fout.close();

  return 0;
}
