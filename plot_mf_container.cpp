#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>

#include "cpp_common.h"
#include "cpp_magnetic_field_container.h"

using namespace std;

int main(int argc, char **argv) {
  if (argc < 4) {
    cerr << "Usage: " << argv[0] << " config_file factor plots_file" << endl;
    return 1;
  } 

  double cx, cy, cz;
  int nx;
  int ny;
  int nz;
  double dx;
  double dy;
  double dz;
  double factor = atof(argv[2]);

  cppLog.open("cpp_stdout.log");
  MagneticFieldContainer *mf = new MagneticFieldContainer();
  int res = mf->readConfigurationFile(argv[1]);
  if (res != 0) {
    cerr << "Error reading file. res=" << res << endl;
    cppLog.close();
    return 1;
  }

  FILE *pipe = popen("gnuplot -persist 2> /dev/null", "w");

  ifstream fin(argv[3], ios::binary);
  while (!fin.eof()) {
    fin >> cx >> cy >> cz >> nx >> ny >> nz >> dx >> dy >> dz;

    for (int z = 0; z < nz; ++z) {
      for (int y = 0; y < ny; ++y) {
        for (int x = 0; x < nx; ++x) {
          double u, v, w, b;
          double px = cx+x*dx;
          double py = cy+y*dy;
          double pz = cz+z*dz;

          mf->getMFVector(cx+x*dx, cy+y*dy, cz+z*dz, u, v, w, b);

          double l = sqrt(u*u+v*v+w*w);
          if (b > 0 && l > 0) { 
            u *= b*factor/l;
            v *= b*factor/l;
            w *= b*factor/l;
            cout << px << " " << py << " " << pz << " " << u << " " << v << " " << w << endl;
          }
        }
      }
    }
  }

  fprintf(pipe, "splot './plot.dat' with vectors\n");
  fprintf(pipe, "show\n");
  pclose(pipe);

  fin.close();

  cppLog.close();

  return 0;
}
