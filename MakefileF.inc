# The name of the produced executable
EXECUTABLE=fluka_cpp

### Filename extensions
#
FEXT_OBJ=.o
FEXT_F=.f
FEXT_DEP=.fd

### Compiler
#
FF=$(FLUPRO)/flutil/fff
FFLINK=$(FLUPRO)/flutil/lfluka

### Directories in the project
#
FDIR_SRC=fsrc/
FDIR_OBJ=obj/
FDIR_LIB=./
FDIR_INCLUDE=./
FDIR_TESTS=./
FDIR_SCRIPTS=./
FDIR_DEP=./
FDIR_BIN=./

FFLAGS=-lstdc++ -m fluka -o $(FDIR_BIN)$(EXECUTABLE)
