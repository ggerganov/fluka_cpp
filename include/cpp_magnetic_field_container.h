#ifndef _CPP_MAGNETIC_FIELD_CONTAINER_H_
#define _CPP_MAGNETIC_FIELD_CONTAINER_H_

#include "cpp_magnetic_field.h"

#include <vector>

typedef struct {
  int id;
  MagneticField *mf;
} MFEntry;

typedef struct {
  double x, y, z;
  MagneticField *mf;
} MFPos;

class MagneticFieldContainer {
  public:
    MagneticFieldContainer() : m_lastpos(0) {};
    ~MagneticFieldContainer() {};

    bool insertMF(const int id, MagneticField *mf);
    MagneticField * getMF(const int id) const;

    bool insertMFPos(const int id, const double x,
                                   const double y,
                                   const double z);

    void getMFVector(const double x,
                     const double y,
                     const double z,
                           double &u, double &v, double &w,
                           double &l);

    int readConfigurationFile(const char *fname);

  private:
    int m_lastpos;

    std::vector<MFEntry> m_mfmap;
    std::vector<MFPos> m_mfarr;
};

#endif
