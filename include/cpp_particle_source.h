#ifndef _CPP_PARTICLE_SOURCE_H_
#define _CPP_PARTICLE_SOURCE_H_

#include <fstream>

#include "cpp_particle.h"
#include "cpp_particle_container.h"

class ParticleSource {
  public:
    ParticleSource() {};

    ~ParticleSource() {};

    int initSource(const char *fname);

    inline Particle* seeNextParticle() { 
      return m_pcontainer.getCurParticlePtr();
    }
    inline void popCurParticle() { m_pcontainer.popCurParticle(); }

    int finishSource();

  private:
    ParticleContainer m_pcontainer;
};

#endif
