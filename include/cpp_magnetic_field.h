#ifndef _CPP_MAGNETIC_FIELD__H_
#define _CPP_MAGNETIC_FIELD__H_

typedef struct {
  double u, v, w, l;
} MagneticFieldVector3D;

class MagneticField {
  public:
    MagneticField() : m_grid(0) {};
    ~MagneticField() {
      if (m_grid) delete [] m_grid;
    };

    bool readFromBinary(const char *fname);
    void getField(const double x, const double y, const double z,
                  double &u, double &v, double &w, double &l);

    inline bool isInside(const double x, const double y, const double z) const {
      if ((x < 0) || (y < 0) || (z < 0) || 
          (x >= (m_nx-1)*m_dx) ||
          (y >= (m_ny-1)*m_dy) ||
          (z >= (m_nz-1)*m_dz) ) return false;
      return true;
    }

  private:
    int m_nx, m_ny, m_nz;
    double m_dx, m_dy, m_dz;

    // Temporary vars used for calculations
    int i1, i2, i3, i4, i5, i6, i7, i8, ix, iy, iz;
    double p1, p2, p3, p4, p5, p6, fx, fy, fz;

    MagneticFieldVector3D *m_grid;
};

#endif
