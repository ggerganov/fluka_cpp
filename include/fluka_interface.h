#ifndef _FLUKA_INTERFACE_H_
#define _FLUKA_INTERFACE_H_

/*! \brief Gives the rest mass of the particle.
 */
#ifndef WIN32
#define am am_
#else
#define am AM
#endif
extern "C" double am(int &type);

/*! \brief Converts region number to region name.

  Input:  \a regNum - number of the region
  Output: \a regName - the alphabetical name of the region with
          number regNum
          \a iErr - error code. 0 - OK, 1 - Not OK
 */
#ifndef WIN32
#define geor2n geor2n_
#else
#define geor2n GEOR2N
#endif
extern "C" void geor2n(int  &regNum,  char *regName, int &iErr);

/*! \brief Converts region name to region number.

  Input:  \a regName - the alphabetical name of the region
  Output: \a regNum - the number of the region with name regName
          \a iErr - error code. 0 - OK, 1 - Not OK
*/
#ifndef WIN32
#define geon2r geon2r_
#else
#define geon2r GEON2R
#endif
extern "C" void geon2r(char *regName, int  &regNum,  int &iErr);

/*! \brief Structure that acts as an interface for fluka to 
    some global variables in c++.
 */
#ifndef WIN32
#define cglobal cglobal_
#else
#define cglobal CGLOBAL
#endif
extern "C" { 
    struct st_cglobal {
    bool isScoring;
    bool isSource;
    bool isMagFld;
  } cglobal;
}

#endif

