#ifndef _CPP_PARTICLE_CONTAINER_H_
#define _CPP_PARTICLE_CONTAINER_H_

#include <vector>
#include <cstring>
#include <fstream>

#include "cpp_particle.h"

#define DEFAULT_PCONTAINER_SIZE 1024

class ParticleContainer {
  public:
    ParticleContainer() :
        m_curParticle(0),
        m_containerSize(DEFAULT_PCONTAINER_SIZE),
        m_totalParticles(0),
        m_data(m_containerSize),
        m_streaming(false),
        m_filename(""),
        m_fin(0),
        m_fout(0) {};
    ParticleContainer(int psize) :
        m_curParticle(0),
        m_containerSize(psize),
        m_totalParticles(0),
        m_data(m_containerSize),
        m_streaming(false),
        m_filename(""),
        m_fin(0),
        m_fout(0) {};

    ~ParticleContainer() {};

    Particle* getCurParticlePtr() { return (m_curParticle < 0) ?
                                           NULL :
                                           &(m_data[m_curParticle]); }
    int getCurParticle() const { return m_curParticle; }

    void scoreCurParticle();
    void popCurParticle();

    int m_getTotalParticles() const { return m_totalParticles; }

    /// Write operations
    int startFileStream(const char* fname);
    int resetFileStream();
    int flushFileStream();
    int finishFileStream();

    /// Read operations
    int openFileStream(const char* fname);
    int readFileStream();
    int closeFileStream();

  private:
    int m_curParticle;
    int m_containerSize;
    int m_totalParticles;

    std::vector<Particle> m_data;

    bool m_streaming;
    std::string m_filename;
    std::ifstream *m_fin;
    std::ofstream *m_fout;
};

#endif
