#ifndef _CPP_PARTICLE_H_
#define _CPP_PARTICLE_H_

typedef struct {
  int    m_type;
  double m_energy;
  double m_w;
  double m_posx;
  double m_posy;
  double m_posz;
  double m_dirx;
  double m_diry;
  double m_dirz;
} ParticleData;

class Particle {
  public:
    Particle() {};
    ~Particle() {};

    ParticleData* getDataPtr() { return &m_data; } 

  private:
    ParticleData m_data; 
};

#endif
