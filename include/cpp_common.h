#ifndef _CPP_COMMON_H_
#define _CPP_COMMON_H_

#define MAX_FLUKA_REGIONS 10000

#include <fstream>

#include "cpp_particle_source.h"
#include "cpp_particle_container.h"

#include "cpp_magnetic_field_container.h"

/*! \brief Add the '../' prefix to a filename. 

 \a fname - result
 \a pfname - file name from fluka input file (if any)
 \a def - default filename if pfname is empty 

 If \a pfname is empty -> return false; else return true;
*/
static bool add_fname_prefix(char *fname, const char *pfname, const char *def) {
  fname[0] = '.';
  fname[1] = '.';
  fname[2] = '/';
  fname[8+3] = '\0';
  for (int i = 0+3; i < 8+3; ++i) {
    if (pfname[i-3] == ' ') {
      fname[i] = '\0';
      if ((i-3) == 0) {
        strcpy(fname, def);
        return false;
      }
      break;
    }
    fname[i] = pfname[i-3];
  }
  return true;
}

extern std::ofstream cppLog;

static std::vector<int>   g_regionsBeingScored;
static ParticleContainer *g_regionScoring[MAX_FLUKA_REGIONS];

static ParticleSource *g_phspSource;

static MagneticFieldContainer *g_mfcontainer;
#endif

