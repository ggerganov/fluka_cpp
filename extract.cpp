#include <iostream>
#include <fstream>

#include "cpp_particle.h"

using namespace std;

int main(int argc, char **argv) {
  if (argc < 2) {
    cerr << "Usage: " << argv[0] << " data_file" << endl;
    return 1;
  }
  
  int n;
  ParticleData p;
  ifstream fin(argv[1], ios::binary);
  if (!fin.good()) {
    cerr << "Unable to open file '" << argv[1] << "' ... " << endl;
    return 2;
  }

  fin.read((char*)(&n), sizeof(n));
  //cout << "n= " << n << endl;
  //cout << "size=" << sizeof(ParticleData) << endl;
  for (int i = 0; i < n; ++i) {
    fin.read((char*)(&p), sizeof(ParticleData));
    cout << p.m_type << " " << p.m_energy << " " << p.m_posx << " " << p.m_posy << " " << p.m_posz << " " <<
                                                    p.m_dirx << " " << p.m_diry << " " << p.m_dirz << endl;
  }
  fin.close();

  return 0;
}
