### Environment
#
# Comment line below to turn off all debug messages
DEBUG=yes
#GPROF=yes

### Filename extensions
#
EXT_OBJ=.co
EXT_LIB=.so
EXT_HH=.h
EXT_CPP=.cpp
EXT_BIN=.exe
EXT_TMPL=.tmpl
EXT_DEP=.d

### Compiler
#
CC=g++

### Directories in the project
#
DIR_SRC=src/
DIR_OBJ=obj/
DIR_LIB=./
DIR_INCLUDE=include/
DIR_TESTS=./
DIR_SCRIPTS=./
DIR_DEP=dep/
DIR_BIN=./

### Compiler flags
#
ifdef DEBUG
  CPPFLAGS=-O3 -W -D DEBUG -g
else
  CPPFLAGS=-O3 -W -ffast-math -D NDEBUG
endif
ifdef GPROF
  CPPFLAGS+=-pg
endif

# Compile 32-bit binary
CPPFLAGS+=-m32

SHAREDFLAGS=-shared -fPIC
