*$ CREATE MAGFLD.FOR
*COPY MAGFLD
*
*===magfld=============================================================*
*
      SUBROUTINE MAGFLD ( X, Y, Z, BTX, BTY, BTZ, B, NREG, IDISC )

      INCLUDE '(DBLPRC)'
      INCLUDE '(DIMPAR)'
      INCLUDE '(IOUNIT)'
*
*----------------------------------------------------------------------*
*                                                                      *
*     Copyright (C) 1988-2010      by Alberto Fasso` & Alfredo Ferrari *
*     All Rights Reserved.                                             *
*                                                                      *
*                                                                      *
*     Created  in     1988         by    Alberto Fasso`                *
*                                                                      *
*                                                                      *
*     Last change on 06-Nov-10     by    Alfredo Ferrari               *
*                                                                      *
*     Input variables:                                                 *
*            x,y,z = current position                                  *
*            nreg  = current region                                    *
*     Output variables:                                                *
*            btx,bty,btz = cosines of the magn. field vector           *
*            B = magnetic field intensity (Tesla)                      *
*            idisc = set to 1 if the particle has to be discarded      *
*                                                                      *
*----------------------------------------------------------------------*
*
      INCLUDE '(CMEMFL)'
      INCLUDE '(CSMCRY)'

      COMMON /CGLOBAL/ isScoring, isSource, isMagFld
      LOGICAL*1 isScoring, isSource, isMagFld

*
*  +-------------------------------------------------------------------*
*  |  Earth geomagnetic field:
      IF ( LGMFLD ) THEN
         CALL GEOFLD ( X, Y, Z, BTX, BTY, BTZ, B, NREG, IDISC )
         RETURN
      END IF
*  |
*  +-------------------------------------------------------------------*
      IDISC = 0

*  If R < 20, use analytical magnetic field:
      GRADIENT = ONEONE
      RADPOS = SQRT(X**2+Y**2)
      IF ( Z > 0 .AND. Z < 50 .AND. RADPOS.LT.20 ) THEN
        B = RADPOS / 20.0
        BTX = Y / RADPOS
        BTY = X / RADPOS
      ELSE
*  If R >= 20, check if FLukaCPP magnetic field module was initialized
*  successfully:
        IF ( isMagFld ) THEN
*  If yes:
*  Call the C++ routines to calculate the magnetic field in this pos
          CALL GETMAGNETICFIELD( X, Y, Z, BTX, BTY, BTZ, B)
        ELSE
*  IF the FlukaCPP magnetic field routine is not initialized then
*  set the magnetic field at this location to 0.
          BTX = 1
          BTY = 0
          BTZ = 0
          B = 0
        END IF
      END IF

      RETURN
*=== End of subroutine Magfld =========================================*
      END

