#!/bin/bash

# Example: Scoring1
# The script runs the simulation in main.inp.
# The scoring.inp file contains the names of the regions
# for which the particles will be scored. After the simulation
# is completed, for each scoring region a binary file is created
# that contains the scored particles entering the region.
# For example, for this simulation, the binary files are called:
#
# ./main001_TARGET
# ./main001_AFTER
#
# The first one contains particles that are entering the water target.
# The second one contains particles that are leaving the water target.
#
# After the simulation is finished, a small program is compiled:
# get_scored_particles.cpp
# The program is then used to read the particles recorded in both files
# and display their parameters.
#
# G. Gerganov

${FLUPRO}/flutil/rfluka -e fluka_cpp -M 1 ./main.inp

g++ -O3 -m32 -o get_scored_particles get_scored_particles.cpp -I../../include

echo ""
echo "[+] Particles entering TARGET ..."
./get_scored_particles *_TARGET

echo ""
echo "[+] Particles leaving TARGET ..."
./get_scored_particles *_AFTER

