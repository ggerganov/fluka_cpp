#!/bin/bash

# Example: Scoring2
# This script runs a simluation in which the primary particles
# are loaded from a binary file. This is achieved by simply adding
# the SOURCE card to the input file of the simulation. If the 
# SDUM parameter of the SOURCE card is set then the particles will
# be read from a file that is named SDUM. Otherwise the default name
# of the source file is used 'source.inp'.
#
# G. Gerganov

${FLUPRO}/flutil/rfluka -e fluka_cpp -M 1 ./main.inp
