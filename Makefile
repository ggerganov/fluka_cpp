### Include the environment variables
#
include MakefileF.inc
include MakefileCPP.inc

### Generate filenames
#
# get the filenames of all the source files and construct the corresponding
# object filenames and dependency filenames
#
SRC_FNAMES=$(wildcard $(DIR_SRC)*$(EXT_CPP))
OBJ_FNAMES=$(patsubst $(DIR_SRC)%$(EXT_CPP),$(DIR_OBJ)%$(EXT_OBJ),$(SRC_FNAMES))
DEP_FNAMES=$(patsubst $(DIR_SRC)%$(EXT_CPP),$(DIR_DEP)%$(EXT_DEP),$(SRC_FNAMES))

FSRC_FNAMES=$(wildcard $(FDIR_SRC)*$(FEXT_F))
FOBJ_FNAMES=$(patsubst $(FDIR_SRC)%$(FEXT_F),$(FDIR_OBJ)%$(FEXT_OBJ),$(FSRC_FNAMES))
FDEP_FNAMES=$(patsubst $(FDIR_SRC)%$(FEXT_F),$(FDIR_DEP)%$(FEXT_DEP),$(FSRC_FNAMES))

### make commands
#
.PHONY : all
all : objs $(FDIR_BIN)$(EXECUTABLE)

.PHONY : objs
objs: $(OBJ_FNAMES) $(FOBJ_FNAMES)

#
# include generated dependencies
#
-include $(DEP_FNAMES)
-include $(DFEP_FNAMES)

#
# rules to compile the object files
#
$(DIR_OBJ)%$(EXT_OBJ) :
	@echo "Compiling object file '$@' ..."
	$(CC) $(CPPFLAGS) -I$(DIR_INCLUDE) -o $@ -c $<

$(FDIR_OBJ)%$(FEXT_OBJ) : $(FDIR_SRC)%$(FEXT_F)
	@echo "Compiling object file '$@' ..."
	$(FF) $<
	@mv -v `echo $@ | sed 's,$(FDIR_OBJ),$(FDIR_SRC),g'` $(FDIR_OBJ)

#
# automatically generate dependency files
#
$(DIR_DEP)%$(EXT_DEP) : $(DIR_SRC)%$(EXT_CPP)
	@echo "Generating dependencies '$@' ..."
	@$(CC) -MM $(CPPFLAGS) -I$(DIR_INCLUDE) $< | sed 's,$*.o,$(DIR_OBJ)$*$(EXT_OBJ),g' > $@

#
# default rule: link all object files
#
$(FDIR_BIN)$(EXECUTABLE): $(OBJ_FNAMES) $(FOBJ_FNAMES)
	$(FFLINK) $(FFLAGS) $(OBJ_FNAMES) $(FOBJ_FNAMES)

### make clean
#
.PHONY : clean
clean :
	-rm -v $(DIR_OBJ)*$(EXT_OBJ)
	-rm -v $(FDIR_OBJ)*$(FEXT_OBJ)
#	-rm -v $(DIR_DEP)*$(EXT_DEP)
