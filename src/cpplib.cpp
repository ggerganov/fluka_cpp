#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>
#include <cmath>
#include <cassert>

#include "cpp_common.h"
#include "fluka_interface.h"
#include "cpp_particle.h"
#include "cpp_particle_container.h"
#include "cpp_magnetic_field.h"
#include "cpp_magnetic_field_container.h"

using namespace std;

#ifndef WIN32
#define initcpp initcpp_
#else
#define initcpp INITCPP
#endif

extern "C" {
  /*! \brief Initialize some global CPP stuff. */
  void initcpp() {
    cppLog.open("cpp_stdout.log");

    cglobal.isScoring = false;
    cglobal.isSource = false;
    cglobal.isMagFld = false;

    cppLog << " [+] CPP stuff initialized successfully." << endl;
  }
}

#ifndef WIN32
#define finishcpp finishcpp_
#else
#define finishcpp FINISHCPP
#endif

extern "C" {
  /*! \brief Finalize. */
  void finishcpp() {
    cppLog << " [+] Finalizing CPP ..." << endl;
    for (unsigned int i = 0; i < g_regionsBeingScored.size(); ++i) {
      g_regionScoring[g_regionsBeingScored[i]]->finishFileStream();
      delete g_regionScoring[g_regionsBeingScored[i]];
    }

    if (g_phspSource) {
      g_phspSource->finishSource();
      delete g_phspSource;
    }

    if (g_mfcontainer) {
      delete g_mfcontainer;
    }

    cglobal.isScoring = false;
    cglobal.isSource = false;
    cglobal.isMagFld = false;

    cppLog << " [+] CPP stuff finalized successfully." << endl;
    cppLog.close();
  }
}

#ifndef WIN32
#define initscoring initscoring_
#else
#define initscoring INITSCORING
#endif

extern "C" {
  /*! \brief Initialize the scoring of particle fluence in user-specified
      regions.

      The routine looks for a file with name 'scoring.inp' and reads the
      user specified regions. A scoring object is created for each of the
      requested regions and particles entering these regions are scored.
   */
  void initscoring () {
    cppLog << " [+] Initializing scoring module." << endl;

    ifstream fin("../scoring.inp");
    if (!fin.good()) {
      cppLog << "     Warning: file 'scoring.inp' is not found or cannot be opened." <<
                "No scoring will be performed in this run." << endl;
      return;
    }

    cppLog << "     Found 'scoring.inp'. Reading scoring regions ..." << endl;

    string line;
    char rname[9];

    fin >> line;
    while (!fin.eof()) {
      unsigned int i;
      for (i = 0; i < line.length(); ++i) {
        if (line[i] == ' ') break;
        rname[i] = line[i];
      }
      for (i = i; i < 8; ++i) {
        rname[i] = ' ';
      }

      int nreg, ierr;
      geon2r_(rname, nreg, ierr);

      if (ierr == 0) {
        cppLog << "     Found region '" << rname << "' : " << nreg << endl;
        if (g_regionScoring[nreg] == 0) {
          cglobal.isScoring = true;
          g_regionScoring[nreg] = new ParticleContainer();
          g_regionScoring[nreg]->startFileStream(line.c_str());
          g_regionsBeingScored.push_back(nreg);
        }
      } else {
        cppLog << "     Region '" << rname << "' not found." << endl;
      }

      fin >> line;
    }

    fin.close();
  }
}

#ifndef WIN32
#define initsource initsource_
#else
#define initsource INITSOURCE
#endif

extern "C" {
  /*! \brief Initialize a particle source that uses phase space files to
      generate new particles. The phase space files can be obtained from
      the fluence scoring module.

      The routine looks for a file with name \a pfname and reads the
      user-specified source options. The routine looks for this file in
      the parent directory, since when the simulation is started with
      $FLUPRO/flutil/rfluka the simulation runs in a temporary subdirectory.
      Therefore in the beggining of the routine we add the '../' prefix
      to the user specified filename.
   */
  void initsource (char *pfname) {  
    cppLog << " [+] Initializing phase-space source module." << endl;

    /// Add the '../' prefix
    char fname[24];
    if (!add_fname_prefix(fname, pfname, "../source.inp")) {
      cppLog << "     User did not specify a filename in the SOURCE card. " <<
                "Using default source filename - 'source.inp'." << endl;
    }

    g_phspSource = new ParticleSource();
    g_phspSource->initSource(fname);
  }
}

#ifndef WIN32
#define initmagfield initmagfield_
#else
#define initmagfield INITMAGFIELD
#endif

extern "C" {
  /*! \brief Initialize user defined magnetic filed maps.
   */
  void initmagfield () {  
    cppLog << " [+] Initializing magnetic field maps." << endl;

    g_mfcontainer = new MagneticFieldContainer();

    /// Add the '../' prefix
    char fname[512];
    add_fname_prefix(fname, " ", "../magnetic.inp");
    cppLog << "     Reading magnetic filed options from '" << fname << "'." << endl;

    ifstream fin(fname);
    if (!fin.good()) {
      cppLog << "     Info: Cannot find magnetic field options file." << endl;
      return;
    }

    while (!fin.eof()) {
      char line[512];
      fin.getline(line, 512);

      bool fl = true;
      for (unsigned int i = 0; i < strlen(line); ++i)
        if (line[i] != ' ') { fl = false; break; }
      if (fl) continue;
      
      if (line[0] == '#' || line[0] == 'C' || line[0] == 'c') {
        // comment
      } else if (line[0] == 'M' || line[0] == 'm') {
        line[0] = ' ';
        stringstream ss; ss << line;
        int id;
        string mfname;
        ss >> id; ss >> mfname;
        cppLog << "     [Magnetic Field] ID: " << id << ", FNAME: '" << mfname << "'" << endl;
        
        string sf("../"); sf += mfname;
        MagneticField *mf = new MagneticField();
        if (mf->readFromBinary(sf.c_str())) {
          if (!g_mfcontainer->insertMF(id, mf)) {
            cppLog << "     ERROR: Field with ID=" << id << " already exist." << endl;
            return;
          }
        } else {
          cppLog << "     ERROR: Unable to read magnetic field file '" << sf << "'" << endl;
          return;
        }
      } else if (line[0] == 'P' || line[0] == 'p') {
        line[0] = ' ';
        stringstream ss; ss << line;
        int id;
        double x, y, z;
        ss >> id; ss >> x >> y >> z;
        cppLog << "     [Position] ID: " << id << ", [" << x << " " << y << " " << z << "]" << endl;

        if (!g_mfcontainer->insertMFPos(id, x, y, z)) {
          cppLog << "     ERROR: Magnetic Field with ID=" << id << " not specified. " << endl;
          return;
        }
      } else {
        cppLog << "     Warning: Unrecognized command: '" << line << "'" << endl;
      }
    }
    fin.close();

    cglobal.isMagFld = true;
    cppLog << "     Done" << endl;
  }
}

#ifndef WIN32
#define scoreparticle scoreparticle_
#else
#define scoreparticle SCOREPARTICLE
#endif

static ParticleData *p_ptr;
extern "C" {
  /*! \brief Scores a single particle in the corresponding region.
   */
  void scoreparticle(int &id, double &ekin, double &mom,
                     double &w, int &nreg,
                     double &posx, double &posy, double &posz,
                     double &dirx, double &diry, double &dirz) {                
    if (g_regionScoring[nreg] == 0) {
      return;
    }

    p_ptr = g_regionScoring[nreg]->getCurParticlePtr()->getDataPtr();
    p_ptr->m_type = id;
    p_ptr->m_energy = ekin;
    p_ptr->m_w = w;
    p_ptr->m_posx = posx;
    p_ptr->m_posy = posy;
    p_ptr->m_posz = posz;
    p_ptr->m_dirx = dirx;
    p_ptr->m_diry = diry;
    p_ptr->m_dirz = dirz;
    g_regionScoring[nreg]->scoreCurParticle();    
  }
}

#ifndef WIN32
#define getprimaryparticle getprimaryparticle_
#else
#define getprimaryparticle GETPRIMARYPARTICLE
#endif

extern "C" {
  /*! \brief Gets the next primary particle from the Phsp source.
   */
  void getprimaryparticle(int &ijbeam, double &pbeam, double &w,
                          double &xbeam, double &ybeam, double &zbeam,
                          double &ubeam, double &vbeam, double &wbeam,
                          int &nomore) { 
    p_ptr = g_phspSource->seeNextParticle()->getDataPtr();
    while (p_ptr != NULL) {
      if (p_ptr->m_type >= -6) break;
      g_phspSource->popCurParticle();
      p_ptr = g_phspSource->seeNextParticle()->getDataPtr();
    }
    if (p_ptr == NULL) {
      nomore = 1;
    } else {
      ijbeam = p_ptr->m_type;
      pbeam = p_ptr->m_energy;
      w     = p_ptr->m_w;
      xbeam = p_ptr->m_posx; 
      ybeam = p_ptr->m_posy; 
      zbeam = p_ptr->m_posz; 
      ubeam = p_ptr->m_dirx; 
      vbeam = p_ptr->m_diry; 
      wbeam = p_ptr->m_dirz;

/*      
      cppLog << p_ptr->m_posx << " " <<
                p_ptr->m_posy << " " <<
                p_ptr->m_posz << " " <<
                p_ptr->m_energy << " " << 
                p_ptr->m_w << " " << 
                p_ptr->m_type << endl;
*/
      
      g_phspSource->popCurParticle();
    }
  }
}

#ifndef WIN32
#define getmagneticfield getmagneticfield_
#else
#define getmagneticfield GETMAGNETICFIELD
#endif

extern "C" {
  /*! \brief Gets the magnetic field at a specified point in space.
   */
  void getmagneticfield(double &x, double &y, double &z,
                        double &u, double &v, double &w, double &l) {
    cppLog << "PLOT " << x << " " << y << " " << z << endl;
    g_mfcontainer->getMFVector(x, y, z, u, v, w, l);
    //cppLog << u << " " << v << " " << w << " " << l << endl;
  }
}
