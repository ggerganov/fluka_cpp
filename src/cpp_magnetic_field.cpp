#include "cpp_magnetic_field.h"

#include <cmath>
#include <fstream>

extern std::ofstream cppLog;

bool MagneticField::readFromBinary(const char *fname) {
  std::ifstream fin(fname, std::ios::binary);

  if (!fin.good()) {
    cppLog << "Error: Unable to open magnetic field file '" << fname <<
              "'. Check your input." << std::endl;
    return false;
  }

  try {
    fin.read((char *)(&m_nx), sizeof(m_nx));
    fin.read((char *)(&m_ny), sizeof(m_ny));
    fin.read((char *)(&m_nz), sizeof(m_nz));

    fin.read((char *)(&m_dx), sizeof(m_dx));
    fin.read((char *)(&m_dy), sizeof(m_dy));
    fin.read((char *)(&m_dz), sizeof(m_dz));

    if (m_grid) { delete [] m_grid; }
    m_grid = new MagneticFieldVector3D[m_nx*m_ny*m_nz];
    for (int i = 0; i < m_nx*m_ny*m_nz; ++i) {
      fin.read((char *)(&m_grid[i].u), sizeof(m_grid[i].u));
      fin.read((char *)(&m_grid[i].v), sizeof(m_grid[i].v));
      fin.read((char *)(&m_grid[i].w), sizeof(m_grid[i].w));
      fin.read((char *)(&m_grid[i].l), sizeof(m_grid[i].l));

      double r = sqrt( m_grid[i].u*m_grid[i].u +
                       m_grid[i].v*m_grid[i].v +
                       m_grid[i].w*m_grid[i].w );

      if (r == 0.0) {
        m_grid[i].u = 0;
        m_grid[i].v = 0;
        m_grid[i].w = 0;
      } else {
        m_grid[i].u /= r;
        m_grid[i].v /= r;
        m_grid[i].w /= r;
      } 
    }
  }
  catch (...) {
    cppLog << "Error: file format of magnetic field in '" << fname << "' is wrong." << std::endl;
    return false;
  }

  return true;
}

void MagneticField::getField(const double x, const double y, const double z,
                             double &u, double &v, double &w, double &l) {
  if (!m_grid) { u = 1; v = 0; w = 0; l = 0; return; }

  if ((x < 0) || (y < 0) || (z < 0) ||
      (x >= (m_nx-1)*m_dx) || (y >= (m_ny-1)*m_dy) || (z >= (m_nz-1)*m_dz)) {
    u = 1; v = 0; w = 0; l = 0; return;
  }

  ix = (int) (x / m_dx);
  iy = (int) (y / m_dy);
  iz = (int) (z / m_dz);

  i1 = iz * m_nx * m_ny +
       iy * m_nx +
       ix;
  i2 = i1 + 1;
  i3 = i1 + m_nx;
  i4 = i3 + 1;
  i5 = i1 + m_nx*m_ny;
  i6 = i5 + 1;
  i7 = i5 + m_nx;
  i8 = i7 + 1;

  fx = x - ix*m_dx;
  fy = y - iy*m_dy;
  fz = z - iz*m_dz;

  // interpoalte U
  p1 = m_grid[i1].u + fx / m_dx * (m_grid[i2].u - m_grid[i1].u);
  p2 = m_grid[i3].u + fx / m_dx * (m_grid[i4].u - m_grid[i3].u);
  p3 = m_grid[i5].u + fx / m_dx * (m_grid[i6].u - m_grid[i5].u);
  p4 = m_grid[i7].u + fx / m_dx * (m_grid[i8].u - m_grid[i7].u);
  p5 = p1 + fy / m_dy * (p2-p1);
  p6 = p3 + fy / m_dy * (p4-p3);
  u = p5 + fz / m_dz * (p6-p5);
  
  // interpoalte V
  p1 = m_grid[i1].v + fx / m_dx * (m_grid[i2].v - m_grid[i1].v);
  p2 = m_grid[i3].v + fx / m_dx * (m_grid[i4].v - m_grid[i3].v);
  p3 = m_grid[i5].v + fx / m_dx * (m_grid[i6].v - m_grid[i5].v);
  p4 = m_grid[i7].v + fx / m_dx * (m_grid[i8].v - m_grid[i7].v);
  p5 = p1 + fy / m_dy * (p2-p1);
  p6 = p3 + fy / m_dy * (p4-p3);
  v = p5 + fz / m_dz * (p6-p5);
  
  // interpoalte W
  p1 = m_grid[i1].w + fx / m_dx * (m_grid[i2].w - m_grid[i1].w);
  p2 = m_grid[i3].w + fx / m_dx * (m_grid[i4].w - m_grid[i3].w);
  p3 = m_grid[i5].w + fx / m_dx * (m_grid[i6].w - m_grid[i5].w);
  p4 = m_grid[i7].w + fx / m_dx * (m_grid[i8].w - m_grid[i7].w);
  p5 = p1 + fy / m_dy * (p2-p1);
  p6 = p3 + fy / m_dy * (p4-p3);
  w = p5 + fz / m_dz * (p6-p5);
  
  // interpoalte L
  p1 = m_grid[i1].l + fx / m_dx * (m_grid[i2].l - m_grid[i1].l);
  p2 = m_grid[i3].l + fx / m_dx * (m_grid[i4].l - m_grid[i3].l);
  p3 = m_grid[i5].l + fx / m_dx * (m_grid[i6].l - m_grid[i5].l);
  p4 = m_grid[i7].l + fx / m_dx * (m_grid[i8].l - m_grid[i7].l);
  p5 = p1 + fy / m_dy * (p2-p1);
  p6 = p3 + fy / m_dy * (p4-p3);
  l = p5 + fz / m_dz * (p6-p5);

  double r = u*u+v*v+w*w;
  if (r == 0) { l = 0; u = 1; }
  else { r = sqrt(r); u /= r; v /= r; w /=r; }
}
