#include "cpp_common.h"
#include "cpp_particle_source.h"

int ParticleSource::initSource(const char *fname) {
  int err = m_pcontainer.openFileStream(fname);
  if (err) {
    cppLog << "Error: Unable to init source from file '" << fname << "'" << std::endl;
    return 1;
  }

  return 0;
}


int ParticleSource::finishSource() {
  int err = m_pcontainer.closeFileStream();
  if (err) {
    cppLog << "Error: Unable to finish source." << std::endl;
    return 1;
  }

  return 0;
}
