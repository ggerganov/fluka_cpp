#include "cpp_particle_container.h"

extern std::ofstream cppLog;

void ParticleContainer::scoreCurParticle() {
  ++m_curParticle;
  if (m_curParticle >= m_containerSize) {
    if (m_streaming) {
      flushFileStream();
    } 
    m_curParticle = 0;
  } 
}

void ParticleContainer::popCurParticle() {
  --m_curParticle;
  if (m_curParticle < 0) {
    m_curParticle = -1;
    if (m_streaming) {
      readFileStream();
    } 
  } 
}

int ParticleContainer::startFileStream(const char* fname) {
  if (m_streaming) {
    cppLog << "Error: Cannot start streaming when it is already started!" << std::endl;
    return 1;
  }

  if (m_fout) {
    m_fout->close();
    delete m_fout;
  }

  m_fout = new std::ofstream(fname, std::ios::binary);
  if (!m_fout || !m_fout->good()) {
    cppLog << "Error, startFileStream(): Cannot open file for particle streaming!" << std::endl;
    return 1;
  }
  m_fout->write((char*)(&m_totalParticles), sizeof(m_totalParticles));

  m_filename = std::string(fname);
  m_streaming = true;

  return 0;
}

int ParticleContainer::resetFileStream() {
  if (!m_streaming) {
    cppLog << "Error: Cannot reset streaming when it is not started!" << std::endl;
    return 1;
  }

  m_streaming = false;

  finishFileStream();
  startFileStream(m_filename.c_str());

  m_streaming = true;

  return 0;
}

int ParticleContainer::flushFileStream() {
  if (!m_streaming) {
    cppLog << "Error: Cannot flush stream when it is not started!" << std::endl;
    return 1;
  }

  for (int i = 0; i < m_curParticle; ++i) {
    m_fout->write((char*)(m_data[i].getDataPtr()), sizeof(ParticleData));
  } 

  m_totalParticles += m_curParticle;

  return 0;
}

int ParticleContainer::finishFileStream() {
  if (!m_streaming) {
    cppLog << "Error: Cannot finish stream when it is not started!" << std::endl;
    return 1;
  }

  flushFileStream();
  m_fout->seekp(std::ios_base::beg);
  m_fout->write((char *)(&m_totalParticles), sizeof(m_totalParticles));
  m_fout->close();

  m_streaming = false;

  return 0;
}

int ParticleContainer::openFileStream(const char* fname) {
  if (m_streaming) {
    cppLog << "Error: Cannot open streaming when it is already started!" << std::endl;
    return 1;
  }

  if (m_fin) {
    m_fin->close();
    delete m_fin;
  }

  m_fin = new std::ifstream(fname, std::ios::binary);
  if (!m_fin || !m_fin->good()) {
    cppLog << "Error, openFileStream(): Cannot open file for particle streaming!" << std::endl;
    return 1;
  }
  m_fin->read((char*)(&m_totalParticles), sizeof(m_totalParticles));

  m_filename = std::string(fname);
  m_streaming = true;

  readFileStream();

  return 0;
}

int ParticleContainer::readFileStream() {
  if (!m_streaming) {
    cppLog << "Error: Cannot read stream when it is not started!" << std::endl;
    return 1;
  }

  int numToRead = (m_totalParticles < ((int) m_data.size())) ?
                  m_totalParticles :
                  m_data.size();
  if (numToRead == 0) {
    m_curParticle = -1;
    return 0;
  }

  for (int i = numToRead-1; i >= 0; --i) {
    m_fin->read((char*)(m_data[i].getDataPtr()), sizeof(ParticleData));
  }

  m_curParticle = numToRead-1;
  m_totalParticles -= numToRead;

  return 0;
}

int ParticleContainer::closeFileStream() {
  if (!m_streaming) {
    cppLog << "Error: Cannot close stream when it is not started!" << std::endl;
    return 1;
  }

  m_fin->close();
  m_streaming = false;

  return 0;
}
