#include "cpp_magnetic_field_container.h"

#include <fstream>
#include <sstream>

extern std::ofstream cppLog;

bool MagneticFieldContainer::insertMF(const int id, MagneticField *mf) {
  for (unsigned int i = 0; i < m_mfmap.size(); ++i) {
    if (m_mfmap[i].id == id) return false;
  }

  MFEntry mfnew;
  mfnew.id = id;
  mfnew.mf = mf;
  m_mfmap.push_back(mfnew);

  return true;
}

MagneticField * MagneticFieldContainer::getMF(const int id) const {
  for (unsigned int i = 0; i < m_mfmap.size(); ++i) {
    if (m_mfmap[i].id == id) return m_mfmap[i].mf;
  }

  return NULL;
}

bool MagneticFieldContainer::insertMFPos(
    const int id,
    const double x,
    const double y,
    const double z) {
  MagneticField *mf = (this->getMF(id));
  if (!mf) return false;

  MFPos newpos;
  newpos.x = x;
  newpos.y = y;
  newpos.z = z;
  newpos.mf = mf;
  m_mfarr.push_back(newpos);

  return true;
}

void MagneticFieldContainer::getMFVector(
    const double x,
    const double y,
    const double z,
          double &u, double &v, double &w,
          double &l) {
  if (m_mfarr[m_lastpos].mf->isInside(x - m_mfarr[m_lastpos].x,
                                      y - m_mfarr[m_lastpos].y,
                                      z - m_mfarr[m_lastpos].z)) {
    m_mfarr[m_lastpos].mf->getField(x - m_mfarr[m_lastpos].x,
                                    y - m_mfarr[m_lastpos].y,
                                    z - m_mfarr[m_lastpos].z, 
                                    u, v, w, l);
    return;
  }

  for (unsigned int i = 0; i < m_mfarr.size(); ++i) {
    if (m_mfarr[i].mf->isInside(x - m_mfarr[i].x,
                                y - m_mfarr[i].y,
                                z - m_mfarr[i].z)) {
      m_mfarr[i].mf->getField(x - m_mfarr[i].x,
                              y - m_mfarr[i].y,
                              z - m_mfarr[i].z,
                              u, v, w, l);
      m_lastpos = i;
      return;
    }
  }

  u = 1; v = 0; w = 0; l = 0;
}

int MagneticFieldContainer::readConfigurationFile(const char *fname) {
  std::ifstream fin(fname);
  if (!fin.good()) { return 1; }

  while (!fin.eof()) {
    char line[512];
    fin.getline(line, 512);

    bool fl = true;
    for (unsigned int i = 0; i < strlen(line); ++i)
      if (line[i] != ' ') { fl = false; break; }
    if (fl) continue;

    if (line[0] == '#' || line[0] == 'C' || line[0] == 'c') {
      // comment
    } else if (line[0] == 'M' || line[0] == 'm') {
      line[0] = ' ';
      std::stringstream ss; ss << line;
      int id;
      std::string mfname;
      ss >> id; ss >> mfname;
      cppLog << "     [Magnetic Field] ID: " << id << ", FNAME: '" << mfname << "'" << std::endl;

      std::string sf(mfname.c_str());
      MagneticField *mf = new MagneticField();
      if (mf->readFromBinary(sf.c_str())) {
        if (!this->insertMF(id, mf)) {
          cppLog << "     ERROR: Field with ID=" << id << " already exist." << std::endl;
          return 2;
        }
      } else {
        cppLog << "     ERROR: Unable to read magnetic field file '" << sf << "'" << std::endl;
        return 3;
      }
    } else if (line[0] == 'P' || line[0] == 'p') {
      line[0] = ' ';
      std::stringstream ss; ss << line;
      int id;
      double x, y, z;
      ss >> id; ss >> x >> y >> z;
      cppLog << "     [Position] ID: " << id << ", [" << x << " " << y << " " << z << "]" << std::endl;

      if (!this->insertMFPos(id, x, y, z)) {
        cppLog << "     ERROR: Magnetic Field with ID=" << id << " not specified. " << std::endl;
        return 4;
      }
    } else {
      cppLog << "     Warning: Unrecognized command: '" << line << "'" << std::endl;
    }
  }

  fin.close();
  return 0;
}
