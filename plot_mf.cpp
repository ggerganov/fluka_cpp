#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>

using namespace std;

int main(int argc, char **argv) {
  if (argc < 3) {
    cerr << "Usage: " << argv[0] << " MFfilename factor" << endl;
    return 1;
  } 

  int nx;
  int ny;
  int nz;
  double dx;
  double dy;
  double dz;
  double factor = atof(argv[2]);

  ifstream fout(argv[1], ios::binary);
  fout.read((char *)(&nx), sizeof(nx));
  fout.read((char *)(&ny), sizeof(ny));
  fout.read((char *)(&nz), sizeof(nz));
  fout.read((char *)(&dx), sizeof(dx));
  fout.read((char *)(&dy), sizeof(dy));
  fout.read((char *)(&dz), sizeof(dz));

  FILE *pipe = popen("gnuplot -persist 2> /dev/null", "w");

  for (int z = 0; z < nz; ++z) {
    for (int y = 0; y < ny; ++y) {
      for (int x = 0; x < nx; ++x) {
        double u;
        double v;
        double w;
        double b;
        fout.read((char *)(&u), sizeof(u));
        fout.read((char *)(&v), sizeof(v));
        fout.read((char *)(&w), sizeof(w));
        fout.read((char *)(&b), sizeof(b));

        double l = sqrt(u*u+v*v+w*w);
        if (l == 0) { b = 0; }
        else {
          u *= b*factor/l;
          v *= b*factor/l;
          w *= b*factor/l;
        }
    
        double cx = x*dx;
        double cy = y*dy;
        double cz = z*dz;
        
        //cout << cx << " " << cy << " " << cz << " " << (cx+u) << " " << (cy+v) << " " << (cz+w) << endl;
        cout << cx << " " << cy << " " << cz << " " << u << " " << v << " " << w << endl;
      }
    }
  }

  fprintf(pipe, "splot './plot.dat' with vectors\n");
  fprintf(pipe, "show\n");
  pclose(pipe);

  fout.close();

  return 0;
}
